﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerState : MonoBehaviour {

	[SerializeField] AudioClip orbGet;
	[SerializeField] AudioClip monsterGet;
	private bool hasMonsterGetPlayed = false;

	AudioSource myAudioPlayerThing;
	AudioSource footsteps;
	Light moonlight;
	Light enemylight;
	Light lampLight;
	int remainingOrbs;
	float totalOrbs;

	public void Start () {
		myAudioPlayerThing = gameObject.GetComponent<AudioSource> ();
		footsteps = gameObject.transform.Find ("MainCamera").GetComponent<AudioSource> ();
		if (GameObject.FindGameObjectWithTag ("Moonlight") != null)
			moonlight = GameObject.FindGameObjectWithTag ("Moonlight").GetComponent<Light> ();
		if (GameObject.FindGameObjectWithTag ("Enemy") != null)
			enemylight = GameObject.FindGameObjectWithTag ("Enemy").transform.Find ("Spotlight").GetComponent<Light> ();
		lampLight = gameObject.GetComponentInChildren<Light> ();
		remainingOrbs = GameObject.FindGameObjectsWithTag ("Orb").Length;
		totalOrbs = remainingOrbs;
	}

	public void OnCollisionEnter (Collision collision) {
		if (hasMonsterGetPlayed == false) {
			if (collision.gameObject.CompareTag ("Enemy")) {
				hasMonsterGetPlayed = true;
				myAudioPlayerThing.clip = monsterGet;
				myAudioPlayerThing.Play ();
				StartCoroutine(LoadLevel("Lobby", monsterGet.length));
			}
		}
	}

	void OnTriggerEnter (Collider other) {
		if(!myAudioPlayerThing.isPlaying) {
			if (other.gameObject.CompareTag("Orb")) {
				myAudioPlayerThing.clip = orbGet;
				myAudioPlayerThing.Play ();
				Destroy(other.gameObject);
				remainingOrbs--;

				lampLight.range = Mathf.Lerp(35f, 15f, remainingOrbs / totalOrbs);
                enemylight.range = Mathf.Lerp (10f, 5f, remainingOrbs / totalOrbs);

				if (remainingOrbs == 0) {
					StartCoroutine(LenseFlair(0.1f));
					StartCoroutine(LoadLevel("Lobby", 5f));
				}
			}
		}
	}

	IEnumerator LoadLevel (string _name, float _delay) {
		yield return new WaitForSeconds (_delay);
		SceneManager.LoadScene (_name, LoadSceneMode.Single);
	}

	IEnumerator LenseFlair (float _delay) {
		yield return new WaitForSeconds (_delay);
		moonlight.intensity += 0.1f;
		StartCoroutine(LenseFlair(_delay));
	}


	public void Update () {
		if (Input.GetButtonDown ("Submit")) {
			SceneManager.LoadScene ("Lobby", LoadSceneMode.Single);
		}

		if (Input.GetButtonDown ("Cancel")) {
			Application.Quit ();
		}
	}


	public void FixedUpdate () {
		if (footsteps) {
			float horizonal = Input.GetAxis ("Horizontal");
			float vertical = Input.GetAxis ("Vertical");
			if ((Mathf.Abs (horizonal) > float.Epsilon || Mathf.Abs (vertical) > float.Epsilon)) {
				// start the footsteps as needed
				if(!footsteps.isPlaying)
					footsteps.Play ();
			} else if(footsteps.isPlaying) {
				// if we aren't moving, stop the footsteps
				footsteps.Stop ();
			}
		}
	}
}
