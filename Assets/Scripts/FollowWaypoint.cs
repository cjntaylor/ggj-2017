﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowWaypoint : MonoBehaviour {

	public GameObject path;
	public float velocity = 1;
	public int index = 1;

	// Zero index is the "parent", so skip it
	private Transform[] waypoints;
	private bool debounce;

	// Use this for initialization
	void Start () {
		waypoints = path.GetComponentsInChildren<Transform> ();
		debounce = false;
	}

	// Update is called once per frame
	void Update () {
		// Rotate to face the waypoint
		Quaternion rotation = Quaternion.LookRotation (waypoints [index].position - transform.position);
		transform.rotation = Quaternion.Slerp (transform.rotation, rotation, Time.deltaTime * 2);

		// Move at constant speed towards the next point
		transform.position = Vector3.MoveTowards (transform.position, waypoints [index].position, Time.deltaTime * velocity);
	}

	void OnTriggerEnter(Collider other) {
		// Ignore collisions with anything but the target waypoint
		if (other.transform != waypoints [index].transform)
			return;

		// Debounce trigger event
		if (debounce) return;
		debounce = true;
		StartCoroutine (NextWaypoint (0.1f));
	}

	private IEnumerator NextWaypoint(float delay) {
		yield return new WaitForSeconds (delay);
		index = ((index+1) == waypoints.Length) ? 1 : index + 1;
		debounce = false;
	}
}
