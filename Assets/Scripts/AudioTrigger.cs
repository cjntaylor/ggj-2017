﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioTrigger : MonoBehaviour {

	AudioSource source;

	// Use this for initialization
	void Start () {
		source = GetComponentInChildren<AudioSource> ();
	}

	void OnTriggerEnter(Collider other) {
		source.mute = false;
	}

	void OnTriggerExit(Collider other) {
		source.mute = true;
	}
}
