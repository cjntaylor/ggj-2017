﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LobbyTrigger : MonoBehaviour {

	void OnTriggerEnter () {
		SceneManager.LoadScene ("Forest", LoadSceneMode.Single);
	}
}
