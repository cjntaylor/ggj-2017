﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomTreeMesh : MonoBehaviour {

	[SerializeField] GameObject[] treeMeshes;

	// Use this for initialization
	void Start () {
		// pick a random tree
		int idx = Random.Range (0, treeMeshes.Length);
		// make it
		GameObject other = Instantiate (treeMeshes[idx], Vector3.zero, Quaternion.identity);
		other.transform.localScale = Vector3.one * 1.8f;
		// move it and give it a random rotation
		other.transform.SetParent (transform, false);
		other.transform.RotateAround (transform.position, Vector3.up, Random.Range(0f, 360f));
	}

//	void OnDrawGizmos () {
//		Gizmos.color = Color.cyan;
//		Gizmos.DrawWireCube (transform.position, Vector3.one * 5);
//	}
}
