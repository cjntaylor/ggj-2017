VisionQuest
===========

This year's theme: Waves

##About
Your world begins in a very dark forest. You are not alone. 
Rely on your hearing to gather the orbs of salvation into your trusty lantern and defeat the monster on your heels!

Navigate using your mouse to change direction and WASD keys for navigation.

##Creators
* Colin Taylor
* Garrett Henderson-Tjarks
* Lewis Pearce
* Ai Chen
* Zachary Holtzman
* Jonathan Conti-Vock


##Credits
* Programming - Colin Taylor, Garrett Henderson-Tjarks, Jon Conti-Vock
* Sound - Ai Chen, Lewis Pearce
* Level Design - Lewis Pearce, Garrett Henderson-Tjarks
* System Design - All
